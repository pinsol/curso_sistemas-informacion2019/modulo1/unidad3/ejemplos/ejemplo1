﻿                                              /** Modulo I - Unidad 3 - Hoja Ejemplos 1 **/
DROP DATABASE IF ejemploprogramacion1;
CREATE DATABASE IF NOT EXISTS ejemploprogramacion1;
USE ejemploprogramacion1;
         
/* 
  Ejemplo 1-
  Procedimiento almacenado que reciba dos números y te indique el mayor de ellos (realizarle con instruccion if,
  con consulta de totales y con una función de MySQL)
*/
                   
-- con if
DELIMITER //
CREATE OR REPLACE PROCEDURE ej1v1(a int, b int)
  BEGIN
    IF a>b THEN
        SELECT a;
    ELSE
        SELECT b;
    END IF;
  END //
DELIMITER ;

CALL ej1v1(12,23);

-- con tabla temporal
DELIMITER //
CREATE OR REPLACE PROCEDURE ej1v2(a int, b int)
  BEGIN
    CREATE TEMPORARY TABLE IF NOT EXISTS numeros(
      numero int
      );
    INSERT INTO numeros (numero) VALUES (a),(b);
    SELECT * FROM numeros;
    SELECT MAX(numero) FROM numeros n;
    DROP TABLE numeros;
  END //
DELIMITER ;

  CALL ej1v2(12,23);

-- con greatest
DELIMITER //
CREATE OR REPLACE PROCEDURE ej1v3(a int, b int)
  BEGIN
    SELECT GREATEST(a,b);
  END //
DELIMITER ;

CALL ej1v3(12,23);


/* 
  Ejemplo 2- 
  Procedimiento almacenado que reciba tres números y te indique el mayor de ellos (realizarle con instruccion if,
  con consulta de totales y con una función de MySQL)
*/
                   
-- con if
DELIMITER //
CREATE OR REPLACE PROCEDURE ej2v1(a int, b int, c int)
  BEGIN
    IF a>b THEN
        IF a>c THEN SELECT a;
        ELSE SELECT c;
        END IF;
    ELSE
        IF a<b THEN SELECT c;
        ELSE SELECT b;
        END IF;
    END IF;
  END //
DELIMITER ;

CALL ej2v1(12,23,55);

-- con tabla temporal
DELIMITER //
CREATE OR REPLACE PROCEDURE ej2v2(a int, b int, c int)
  BEGIN
    CREATE TEMPORARY TABLE IF NOT EXISTS numeros(
      numero int
      );
    INSERT INTO numeros (numero) VALUES (a),(b),(c);
    SELECT * FROM numeros;
    SELECT MAX(numero) FROM numeros n;
    DROP TABLE numeros;
  END //
DELIMITER ;

  CALL ej2v2(12,23,55);

-- con greatest
DELIMITER //
CREATE OR REPLACE PROCEDURE ej2v3(a int, b int, c int)
  BEGIN
    SELECT GREATEST(a,b,c);
  END //
DELIMITER ;

CALL ej2v3(12,23,55);

/*
  Ejemplo 3- 
  Procedimiento almacenado que reciba tres números y dos argumentos de tipo salida
  donde devuelva el numero más grande y el número más pequeño de los tres números pasados
*/
DELIMITER //
CREATE OR REPLACE PROCEDURE
    ej3(a int, b int,c int, OUT d int, OUT e int)
  BEGIN
    SET d=GREATEST(a,b,c);
    SET e=LEAST(a,b,c);
  END //
DELIMITER ;

CALL ej3(12,23,55,@v1, @v2);
SELECT @v1;
SELECT @v2;

/*
  Ejemplo 4- 
  Procedimiento almacenado que reciba dos fechas y te muestre el número de días de
  diferencia entre las dos fechas
*/
DELIMITER //
CREATE OR REPLACE PROCEDURE ej4(fecha1 date, fecha2 date)
  BEGIN
    DECLARE resultado int;
  
    set resultado=DATEDIFF(fecha1,fecha2);
  
    SELECT ABS(resultado) dias;
  END //
DELIMITER ;

CALL ej4('2010/1/1','2010/1/10');

/* Ejemplo 5-
   Procedimiento almacenado que reciba dos fechas y te muestre el número de meses de
   diferencia entre las dos fechas
*/
DELIMITER //
CREATE OR REPLACE PROCEDURE ej5(fecha1 date, fecha2 date)
  BEGIN
    DECLARE resultado int;
    
    set resultado=MONTH(fecha1)-MONTH(fecha2);

    SELECT ABS(resultado) meses;
  END //
DELIMITER ;

CALL ej5('2010/1/1','2010/1/10');

/* Ejemplo 6-
   Procedimiento almacenado que reciba dos fechas y te devuelve en 3 argumentos de
   salida los días, meses y años entre las dos fechas
*/
DELIMITER //
CREATE OR REPLACE PROCEDURE ej6(fecha1 date, fecha2 date, OUT dias int, OUT meses int, OUT annos int)
  BEGIN
    SET dias=TIMESTAMPDIFF(DAY,fecha1,fecha2);
    SET meses=TIMESTAMPDIFF(MONTH,fecha1,fecha2);
    SET annos=TIMESTAMPDIFF(YEAR,fecha1,fecha2);
  END //
DELIMITER ;

CALL ej6('2010/1/1','2010/1/10',@d,@m,@a);

SELECT @d, @m, @a;

/* Ejemplo 7-
  Procedimiento almacenado que reciba un frase y te muestre el número de caracteres
*/
DELIMITER //
CREATE OR REPLACE PROCEDURE ej7(frase varchar(150))
  BEGIN
    SELECT CHAR_LENGTH(frase);
  END //
DELIMITER ;

CALL ej7('ejemplo de una frase');